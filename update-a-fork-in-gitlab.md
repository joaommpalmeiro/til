# Update a fork in GitLab

```bash
git remote -v
```

```bash
git remote add upstream https://gitlab.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git
```

(e.g., `https://gitlab.com/ansol/web-ansol.org.git`)

```bash
git remote -v
```

```bash
git fetch upstream
```

```bash
git merge upstream/ORIGINAL_DEFAULT_BRANCH
```

(e.g., `git merge upstream/master`)

```bash
git push
```

The password is the personal access token.

## References

- https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/working-with-forks/configuring-a-remote-repository-for-a-fork?platform=linux
- https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/working-with-forks/syncing-a-fork#syncing-a-fork-branch-from-the-command-line
- https://stackoverflow.com/questions/3903817/pull-new-updates-from-original-github-repository-into-forked-github-repository
- https://bassistance.de/2010/06/25/git-fu-updating-your-github-fork/
