# til

Today I Learned (TIL).

| Tool   | TIL                                                                                                                                                                                                                 |
| ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Chrome | [Copy all bookmarks as individual links from a folder in Chrome](copy-all-bookmarks-as-individual-links-from-a-folder-in-chrome.md)                                                                                 |
| conda  | [Create an `environment.yml` file with dependencies in `requirements.txt` files and a custom package index](create-an-environmentyml-file-with-dependencies-in-requirementstxt-files-and-a-custom-package-index.md) |
| conda  | [Create an `environment.yml` file with dependencies in one or more `requirements.txt` files](create-an-environmentyml-file-with-dependencies-in-one-or-more-requirementstxt-files.md)                               |
| degit  | [Clear degit cache](clear-degit-cache.md)                                                                                                                                                                           |
| fnm    | [Install Node.js (via fnm) on macOS](install-nodejs-via-fnm-macos.md)                                                                                                                                               |
| fnm    | [Uninstall fnm on macOS](uninstall-fnm-on-macos.md)                                                                                                                                                                 |
| GitLab | [Update a fork in GitLab](update-a-fork-in-gitlab.md)                                                                                                                                                               |
| macOS  | [Disable the Quick Note hot corner](disable-quick-note-hot-corner.md)                                                                                                                                               |
| Slack  | [Change the default emojis when hovering over a message in Slack](change-the-default-emojis-when-hovering-over-a-message-in-slack.md)                                                                               |
