# Disable the Quick Note hot corner

```bash
defaults write com.apple.dock wvous-br-corner -int 0 && killall Dock
```

```bash
defaults read com.apple.dock wvous-br-corner
```

## References

- https://blog.jiayu.co/2018/12/quickly-configuring-hot-corners-on-macos/
- https://github.com/mathiasbynens/dotfiles/blob/66ba9b3cc0ca1b29f04b8e39f84e5b034fdb24b6/.macos#L436
- https://support.apple.com/en-gb/guide/mac-help/mchlp3000/12.0/mac/12.0
- https://support.apple.com/en-gb/guide/notes/apdf028f7034/4.9/mac/12.0
- https://macos-defaults.com/dock/tilesize.html
- https://github.com/mathiasbynens/dotfiles/blob/66ba9b3cc0ca1b29f04b8e39f84e5b034fdb24b6/.macos#L924
