# Copy all bookmarks as individual links from a folder in Chrome

1. Open the bookmark manager (`chrome://bookmarks/`).
2. Open the folder where the bookmarks you want to copy are located.
3. Click on one of the bookmarks.
4. Windows: <kbd>Ctrl</kbd> + <kbd>A</kbd>
5. Windows: <kbd>Ctrl</kbd> + <kbd>C</kbd>

## References

- https://superuser.com/a/623473
