# Clear degit cache

```bash
ls -R ~/.degit/
```

```bash
rm -rf ~/.degit/
```

## References

- https://github.com/Rich-Harris/degit?tab=readme-ov-file#degit-straightforward-project-scaffolding
- https://github.com/Rich-Harris/degit/blob/v2.8.4/src/index.js#L93
- https://github.com/Rich-Harris/degit/blob/v2.8.4/src/index.js#L306
- https://github.com/Rich-Harris/degit/blob/v2.8.4/src/index.js#L420
- https://github.com/Rich-Harris/degit/blob/v2.8.4/src/utils.js#L127
- https://github.com/sindresorhus/home-or-tmp
- https://nodejs.org/api/os.html#oshomedir
- https://github.com/tiged/tiged?tab=readme-ov-file#known-bugs-and-workarounds
