# Uninstall fnm on macOS

Remove the fnm-related configuration from `~/.zshrc`.

```bash
rm -rf ~/Library/Application\ Support/fnm
```

```bash
rm -rf ~/Library/Caches/fnm_multishells
```

```bash
brew uninstall fnm
```

```bash
brew cleanup --prune=all --verbose
```

## Notes

- If you have Homebrew installed for each architecture (`arm64` and `i386`/`x86_64`) on macOS and fnm installed on each of them, run the last two commands for each installation.

### Default configuration to remove from `~/.zshrc`

```bash
# fnm
export PATH="/Users/joaopalmeiro/Library/Application Support/fnm:$PATH"
eval "`fnm env`"
```

## References

- https://github.com/Schniz/fnm#removing
