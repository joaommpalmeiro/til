# Notes

- https://github.com/thoughtbot/til: TIL or TILs
- https://github.com/prettier/prettier/issues/6043 + https://prettier.io/docs/en/ignore.html#markdown
- https://slugify.online/

## Snippets

### `.vscode/settings.json` file

```json
{
  "git.enabled": true,

  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.formatOnSave": true,
  "files.autoSave": "off",

  "[json]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "json.format.enable": false
}
```
