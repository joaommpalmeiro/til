# Change the default emojis when hovering over a message in Slack

- Click on the profile picture in the upper right corner.
- _Preferences_ > _Messages & media_ > _Emoji_ > Check _Show one-click reactions on messages_ > Check _Custom_
- Choose the three emoji options.

## Notes

- In the [Slack app](https://slack.com/downloads/mac) on macOS, press <kbd>⌘</kbd> + <kbd>,</kbd> to open _Preferences_.

## References

- https://slack.com/help/articles/4406393601683-Manage-emoji-preferences
