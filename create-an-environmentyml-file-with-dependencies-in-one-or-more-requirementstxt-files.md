# Create an `environment.yml` file with dependencies in one or more `requirements.txt` files

Assuming all files will be in the same folder, create an `environment.yml` file:

<!-- prettier-ignore-start -->
```yml
name: name-dev
channels:
  - conda-forge
dependencies:
  - python=3.8.16
  - pip
  - pip:
    - -r requirements.txt
    - -r requirements-dev.txt
```
<!-- prettier-ignore-end -->

## References

- https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file
- https://pip.pypa.io/en/stable/reference/requirements-file-format/
- https://github.com/conda/conda/tree/main/tests/conda_env/support/advanced-pip
- https://stackoverflow.com/a/59056234
