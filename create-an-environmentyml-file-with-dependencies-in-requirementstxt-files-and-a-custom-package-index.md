# Create an `environment.yml` file with dependencies in `requirements.txt` files and a custom package index

Assuming all files will be in the same folder, create an `environment.yml` file:

<!-- prettier-ignore-start -->
```yml
name: name-dev
channels:
  - conda-forge
dependencies:
  - python=3.9.16
  - pip
  - pip:
    - --index-url http://127.0.0.1/pypi/simple/
    # In case the repository is not available via HTTPS:
    - --trusted-host 127.0.0.1
    - -r requirements.txt
    - -r requirements-dev.txt
```
<!-- prettier-ignore-end -->

## References

- https://github.com/conda/conda/tree/main/tests/conda_env/support/advanced-pip
- https://pip.pypa.io/en/stable/cli/pip_install/#cmdoption-0
- https://en.wikipedia.org/wiki/Localhost
