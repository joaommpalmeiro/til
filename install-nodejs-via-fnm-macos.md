# Install Node.js (via fnm) on macOS

Install fnm:

```bash
curl -fsSL https://fnm.vercel.app/install | zsh
```

```bash
source ~/.zshrc
```

```bash
fnm --version
```

```bash
fnm install 16.19.0
```

```bash
fnm list
```

```bash
node --version
```

## References

- https://github.com/Schniz/fnm
- https://nodejs.org/en/download/package-manager/#fnm
- https://nodejs.org/en/blog/release/
